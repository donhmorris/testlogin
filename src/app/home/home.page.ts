import {Component, OnInit} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/compat/auth';
import {ToastController} from '@ionic/angular';

@Component( {
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
} )
export class HomePage implements OnInit {
  email = '';
  uid: string;
  pwd = '';

  constructor( public auth: AngularFireAuth, protected toast: ToastController ) {
  }

  ngOnInit(): void {
    this.auth.onAuthStateChanged( user => {
      if ( user ) {
        this.email = user.email;
        this.uid = user.uid;
      } else {
        this.uid = undefined;
      }
    } );
  }

  async doLogin() {
    try {
      await this.auth.signInWithEmailAndPassword( this.email, this.pwd );
    } catch ( err ) {
      const toast = await this.toast.create( {
        position: 'top',
        color: 'danger',
        header: 'Login Error',
        message: err.message,
        duration: 2500
      } );

      await toast.present();
    }
  }

  async doLogout() {
    await this.auth.signOut();
  }

}













